import React, { Component } from 'react';
import logo from './assets/images/tubekids_image2.png';
import './assets/css/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { setJWT }from './Utilities';
// Import Components
import Login from './components/Login';
import LoginK from './components/LoginK';
import Register from './components/Register';
import MenuBar from './components/MenuBar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Kid from './components/Kid';
import Video from './components/Video';
import DashboardK from './components/DashboardK';
class App extends  Component {
  constructor(){
    super();
    this.state = {
      "auth":{
        logged: false,
        token:'',
        user: {}
      }
    };
    this.setAuth = this.setAuth.bind(this);
  } // Constructor
  setAuth(token,user){
    setJWT(token);
    this.setState({
      auth:{
        logged: token && true,
        token: token,
        user:user
      }
    });
  }
  render(){
    return (
      <Router>
        <div className="App">
          {/* Adding menu bar */}
          <MenuBar/>
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/login" render={()=>(<Login auth={this.state.auth} setAuth={this.setAuth}/>)}/>
            <Route path="/logink" component={LoginK}/>
            <Route path="/register" component={Register}/>
            <Route path="/dashboard" component={Dashboard}/>
            <Route path="/kid" component={Kid}/>
            <Route path="/video" component={Video}/>
            <Route path="/dashboardk" component={DashboardK}/>
          </Switch>        
        </div>
      </Router>
    );
  }
}
const Home = () =>(
  <div>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <img src={logo} alt="Logo" style={{maxWidth: "80%"}}/>
          </div>
          <div className="col-md-12 mt-5">
            <a href="/login"  type="button" className="btn btn-lg btn-success text-uppercase mr-3">Iniciar Sesión</a>
            <a href="/register" type="button" className="btn btn-lg btn-secondary text-uppercase">Registrarse</a>
          </div>
          <div className="col-md-12 mt-5">
            <a href="/logink" type="button" className="btn btn-lg btn-primary text-uppercase">Inicio de Sesión niños</a>
          </div>
        </div>
      </div>
  </div>
    
  
);

export default App;
