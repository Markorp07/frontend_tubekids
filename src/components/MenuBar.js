import React, {Component} from 'react';
import logo from '../assets/images/tubekids_image2.png';
class MenuBar extends Component{

    constructor(){
        super();
        //Initial state
        
    }  
    onLogoutClick(){
        window.location.href = "/";
        localStorage.removeItem("user");
        localStorage.removeItem("kid");
    }
    route(){
        if(localStorage.getItem("user")){
            return  "/dashboard";
        }else{
            return "/"
        }
    }
    render(){
        return(
            <React.Fragment>
                <nav className="navbar navbar-dark bg-dark mb-4">
                    <a href={ this.route() } className="navbar-brand"><img src={logo} alt="Logo" style={{maxWidth: "100px"}}/></a>
                    <div className="nav navbar-nav navbar-right">
                    <button id="logout" className="btn btn-dark btn-block text-uppercase" onClick={this.onLogoutClick}> 
                        Cerrar Sesión
                    </button>
                    </div>
                </nav>
            </React.Fragment>
        );
    }
}

export default MenuBar;