import React, {Component} from 'react';
import { naxios } from '../Utilities';
import Swal from 'sweetalert2';
class LoginK extends Component{
    constructor(){
        super();
        // Initial state
        this.state = {
            username:'',
            pin:'',
        };
        // Autobinding
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onLoginClick = this.onLoginClick.bind(this);
    }
    onChangeHandler(e){
        const {name,value} = e.target;
        this.setState({...this.state,[name]:value});
    }
    onLoginClick(e){
        
        // if(this.state.email === '' || this.state.password ===''){
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Error',
        //         text: 'El email o la contraseña son incorrectos.'
        //     });
        // }
        naxios.post('/logink',this.state)
            .then( ({data, status})=>{
                // console.log(data)
                localStorage.setItem("kid", JSON.stringify(data))
                window.location.href = "/DashboardK";
            })
            .catch( (err)=>{console.log(err)});
    }
    render(){
        const round_style = {
            border:0,
            borderRadius: "1rem"
        };
        console.log(this.props.auth);
        return(
            <React.Fragment>
                <div className="container">
                    <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card my-5 card shadow-lg p-3 mb-5 bg-white" style={round_style}>
                        <div className="card-body">
                            <h2 className="card-title text-center">Inicio de Sesión Niños</h2>
                            <hr className="my-4" />
                            {/* <form className="form-signin"> */}
                            <div className="form-group">
                                <input type="username" id="username" className="form-control rounded-pill mb-4" value={this.state.email} name="username" placeholder="Usuario" onChange={this.onChangeHandler} required/>
                                
                            </div>

                            <div className="form-group">
                                <input type="pin" id="pin" className="form-control rounded-pill mb-5" placeholder="Pin" value={this.state.password} name="pin" onChange={this.onChangeHandler} required/>
                            </div>
                            <button className="btn btn-lg btn-success btn-block text-uppercase rounded-pill" onClick={this.onLoginClick} >Ingresar</button>
                            
                            {/* </form> */}
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default LoginK;