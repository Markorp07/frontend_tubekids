import React, { Component } from 'react';	
import { naxios } from '../Utilities';
import Swal from 'sweetalert2';
import {Modal, Button} from 'react-bootstrap';
class Kid extends Component {
    constructor(props) {
        super();
        if(localStorage.getItem("user")){
            let user = JSON.parse(localStorage.getItem("user"));
            var data1 = user.user._id;
        }
        this.state = {
            name:'',
            username:'',
            pin:'',
            age:'',
            userID:data1,
            Kid:[],
            show : false,
            nameModal: '',
            userModal:'',
            pinModal:'',
            ageModal:'',
            idModal:'', // This id is from the kid that I'm going to update
        };
        // Autobinding
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
    }
    async componentDidMount(){
        if(!localStorage.getItem("user")){
            window.location.href = "/Login";
        }else{
            this.getKid().then(res =>{
                const Kid = res.data;
                this.setState({Kid});
                // console.log(Kid);
            });
        }
    }
    onChangeHandler(e){
        const {name,value} = e.target;
        this.setState({...this.state,[name]:value});  
    }
    handleModal(id,name,username,pin,age){
        this.setState({show:!this.state.show})
        this.setState({idModal:id})
        this.setState({nameModal:name})
        this.setState({userModal:username})
        this.setState({pinModal:pin})
        this.setState({ageModal:age})
    }
    onCreateClick(){
        if(!this.validateFill()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe rellenar todos los espacios en blanco.'
            });
        }else if(!this.validateUser()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El usuario ya existe.'
            });
        }else if(!this.validateNumber()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El pin debe ser numérico.(Únicamente 6 dígitos)'
            });
        }else{
            naxios.post('/kid',this.state)
            .then( ({data, status})=>{
                // console.log(data)
            })
            .catch( (err)=>{console.log(err)});
            document.getElementById("name").value = "";
            document.getElementById("username").value = "";
            document.getElementById("pin").value = "";
            document.getElementById("age").value = "";
            window.location.href = "/Kid";
        }
    }
    deleteKid(id){
        Swal.fire({
            title: '¿Seguro que desea eliminar el perfíl seleccionado?',
            text: "Esta acción es irreversible.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Borrar'
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                '¡Borrado!',
                'El perfíl fue borrado correctamente.',
                'success'
              )
            //   {console.log(id)}
                return naxios.delete('/kid'+"/"+id)
                .then(function(response){
                    window.location.href = "/kid";
                    return response;
                })
                .catch(function(error){
                    return error;
                });
            }
          })
    }
    getKid(){
        let user = JSON.parse(localStorage.getItem("user"));
        var id = user.user._id;
        return naxios.get('/kid'+"/"+id)
            .then(function(response){
                return response;
            })
            .catch(function(error){
                return error;
            });
    }
    editKid(id){
        // alert(id + " url " + this.state.urlModal);
        var data = {
            name : this.state.nameModal,
            username : this.state.userModal,
            pin : this.state.pinModal,
            age : this.state.ageModal
        }
        if(!this.validateModalFill()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe rellenar todos los espacios en blanco.'
            });
        }else if(!this.validateModalNumber()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El pin debe ser numérico.(Únicamente 6 dígitos)'
            });
        }else{
            return naxios.put('/kid'+"/"+id, data)
                .then(function(response){
                    window.location.href = "/Kid";
                    return response;
                })
                .catch(function(error){
                    return error;
                });
            }
    }
    validateModalFill(){
        let nameModal = document.getElementById("nameModal").value;
        let userModal = document.getElementById("userModal").value;
        let pinModal = document.getElementById("pinModal").value;
        let ageModal = document.getElementById("ageModal").value;
        if(nameModal === "" || userModal === "" || pinModal === "" || ageModal === "" ){
            return false;
        }else{
            return true;
        }
    }
    validateFill(){
        let name = document.getElementById("name").value;
        let username = document.getElementById("username").value;
        let pin = document.getElementById("pin").value;
        let age = document.getElementById("age").value;
        if(name === "" || username === "" || pin === "" || age === "" ){
            return false;
        }else{
            return true;
        }
    }
    validateNumber(){
        let pin = document.getElementById("pin").value;
        if(isNaN(pin)){
            return false;
        }else if(pin.length < 6 || pin.length > 6){
            return false;
        }else{
            return true;
        }
    }
    validateModalNumber(){
        let pin = document.getElementById("pinModal").value;
        if(isNaN(pin)){
            return false;
        }else if(pin.length < 6 || pin.length > 6){
            return false;
        }else{
            return true;
        }
    }
    validateUser(){
        let username = this.state.username;
        console.log(username);
        return naxios.get('/verifyk',username)
            .then(function(response){
                return response;
            })
            .catch(function(error){
                return error;
            });
    }
    render() {
        const round_style = {
            border:0,
            borderRadius: "1rem"
        };
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="text-center">Perfiles</h2>
                            <hr className="my-4" />
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="card my-2 card shadow-lg mb-5 bg-white" style={round_style}>
                                <div className="card-body">
                                    <h2 className="text-center mb-4">Crear perfil del niño</h2 >
                                        <div className="form-group">
                                            <input type="text" id="name" name="name" placeholder="Nombre Completo" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" id="username" name="username" placeholder="Usuario" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" id="pin" name="pin" placeholder="Pin" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                                        </div>
                                        <div className="form-group">
                                            <input type="number" id="age" name="age" placeholder="Edad" min="1" max="99" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                                        </div>
                                        <button className="btn btn-primary btn-block rounded-pill text-uppercase" onClick={this.onCreateClick}> 
                                        Agregar 
                                        </button>
                                </div>
                            </div>
                        </div>
                        {/* Table list */}
                        <div className="col-md-7">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Usuario</th>
                                        <th>Pin</th>
                                        <th>Edad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.state.Kid.map(kid=>
                                        <tr>
                                            <td>{kid._id}</td>
                                            <td>{kid.name}</td>
                                            <td>{kid.username}</td>
                                            <td>{kid.pin}</td>
                                            <td>{kid.age}</td>
                                            <td><button id="edit" type="button" className="btn btn-info" 
                                            onClick={()=>{this.handleModal(kid._id,kid.name,kid.username,kid.pin,kid.age)}}>
                                                Editar</button></td>

                                            <td><button id="delete" type="button" className="btn btn-danger" onClick={this.deleteKid.bind(this, kid._id)}>Eliminar</button></td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                        {/* Modal */}
                        <Modal show={this.state.show} onHide={()=>this.handleModal()}>
                            <Modal.Header closeButton>
                                <h3 className="text-center">Actualizar Datos</h3>
                            </Modal.Header>
                            <Modal.Body>
                            <div className="form-group">
                                <input type="text" id="nameModal" value={this.state.nameModal} name="nameModal" placeholder="Nombre completo" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                            </div>
                            <div className="form-group">
                                <input type="text" id="userModal" value={this.state.userModal} name="userModal" placeholder="Usuario" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                            </div>
                            <div className="form-group">
                                <input type="text" id="pinModal" value={this.state.pinModal} name="pinModal" placeholder="Pin" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                            </div>
                            <div className="form-group">
                                <input type="number" id="ageModal" value={this.state.ageModal} name="ageModal" placeholder="Edad" min="1" max="99" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                            </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button className="btn btn-success btn-block rounded-pill text-uppercase" onClick={this.editKid.bind(this, this.state.idModal)}>Actualizar</Button>
                                <Button className="btn btn-danger btn-block rounded-pill text-uppercase" onClick={()=>{this.handleModal()}}>Cancelar</Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Kid