import React, { Component } from 'react';
import {naxios} from '../Utilities';
import {Modal, Button} from 'react-bootstrap';
import ReactPlayer from 'react-player';
class DashboardK extends Component {
    constructor(props) {
        super();
        if(localStorage.getItem("kid")){
            let kid = JSON.parse(localStorage.getItem("kid"));
            var data = kid.kid.userID;
        }
        this.state = {
            name:'',
            url:'',
            userID:data,
            Video: [],
            show : false,
            urlModal:'',
        };
    }
    componentDidMount(){
        if(!localStorage.getItem("kid")){
            window.location.href = "/LoginK";
        }else{
            this.getVideo().then(res =>{
                const Video = res.data;
                this.setState({Video});
                // console.log(Video);
            });
        }
    }
    getVideo(){
        let kid = JSON.parse(localStorage.getItem("kid"));
        var id = kid.kid.userID;
        return naxios.get('/video'+"/"+id)
            .then(function(response){
                return response;
            })
            .catch(function(error){
                return error;
            });
    }
    handleModal(url){
        this.setState({show:!this.state.show})
        this.setState({urlModal:url})
    }
 
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                        <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th></th>                                      
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.Video.map(video=>
                                        <tr>
                                            <td>{video.name}</td>
                                            <td><button id="edit" type="button" className="btn btn-success text-uppercase" onClick={()=>{this.handleModal(video.url)}}>Ver Video</button></td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                        {/* Modal */}
                        <Modal show={this.state.show} size="lg" centered animation={false} onHide={()=>this.handleModal()}>
                            <Modal.Header closeButton>
                            </Modal.Header>
                            <Modal.Body>
                                <ReactPlayer url={this.state.urlModal} width="100%" controls={true} playing />
                            </Modal.Body>
                        </Modal>
                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default DashboardK