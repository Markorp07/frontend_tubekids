import React, {Component} from 'react';
import logo from '../assets/images/tubekids_image.png';
import Swal from 'sweetalert2';
import { naxios } from '../Utilities';
class Register extends Component{
    constructor(){
        super();
        //Initial state
        this.state = {
            name:'',
            lastname:'',
            email:'',
            password:'',
            country:'',
            birth:'',
            phone:'',
        };
        // Autobinding
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onRegisterClick = this.onRegisterClick.bind(this);
    }
    onChangeHandler(e){
        const {name,value} = e.target;
        this.setState({...this.state,[name]:value});
    }
    onRegisterClick(){
        if(!this.validatePassword()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Las contraseñas no coinciden.'
            });
        }else if(!this.validateMail()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Las direccción del correo electrónico debe ser real.'
            });
        }else if(!this.validateNumber()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe introducir un número de teléfono válido. Solo números.'
            });
        }else if(!this.validateAge()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe ser mayor de 18 años para registrarse.'
            });
        }else if(!this.validateCountry()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe seleccionar un país.'
            });
        }else if(!this.validateFill()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe rellenar todos los espacios en blanco.'
            });
        }else{
            naxios.post('/register',this.state)
                .then( ({data, status})=>{
                    console.log(data)
                })
                .catch( (err)=>{console.log(err)});
        
                Swal.fire({
                    icon: 'success',
                    title: 'Usuario registrado correctamente',
                    html:
                    'Se envió un enlace de confirmación a su correo.',
                    confirmButtonText:'Aceptar',
                    onClose: () => {
                        window.location.href = "/Login";
                    }
                })
        }
    }
    validateFill(){
        let name = document.getElementById("name").value;
        let lastname = document.getElementById("lastname").value;
        let pass1 = document.getElementById("passwd").value;
        let pass2 = document.getElementById("passwd2").value;
        let birth = document.getElementById("birth").value;
        let phone = document.getElementById("phone").value;
        if(name === "" || lastname === "" || pass1==="" || pass2==="" ||birth==="" || phone===""){
            return false;
        }else return true;
    }
    validatePassword(){
        let pass1 = document.getElementById("passwd").value;
        let pass2 = document.getElementById("passwd2").value;
        if(pass1 !== pass2){
            return false;
        }else return true;
    }
    validateMail(){
        let email = document.getElementById("email").value;
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email) !== true){
            return false;
        }else return true;
    }
    validateNumber(){
        let phone = document.getElementById("phone").value;
        if(isNaN(phone)){
            return false;
        }else if(phone.length < 8 || phone.length > 8){
            return false;
        }else{
            return true;
        }
    }
    validateAge(){
        let birth = document.getElementById("birth").value;
        let age = this.calculateAge(birth);
        if(age < 18){
            return false;
        }else return true;

    }
    calculateAge(nbirth){
        let birth = new Date(nbirth);
        let actualDate = new Date();
        let month = actualDate.getMonth();
        let day = actualDate.getDate();
        let year = actualDate.getFullYear();
        let age;
        actualDate.setDate(day);
        actualDate.setMonth(month);
        actualDate.setFullYear(year);

        age = Math.floor(((actualDate - birth)/(1000*60*60*24)/365));
        return age;
    }
    validateCountry(){
        let country = document.getElementById("country").value;
        if(country === ""){
            return false;
        }else return true;
    }
    render(){
        const round_style = {
            border:0,
            borderRadius: "1rem"
        };
        return(
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="card shadow-lg p-3 mb-5 bg-white" style={round_style}>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div>
                                        <img alt="TubeKids" className="img-fluid mt-5" src={logo}/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div>
                                            <h2 className="text-center" style={{color: "black"}}>Registro de usuarios</h2>
                                            <hr/>
                                            {/* <form action="/add" method="POST"> */}
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <input type="text" id="name" name="name" placeholder="Nombre" onChange={this.onChangeHandler} className="form-control rounded-pill" required/>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <input type="text" id="lastname" name="lastname" placeholder="Apellidos" onChange={this.onChangeHandler}className="form-control rounded-pill" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" id="email" name="email" placeholder="Correo electrónico" onChange={this.onChangeHandler}className="form-control rounded-pill" required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" id="passwd"name="password" placeholder="Contraseña" onChange={this.onChangeHandler}className="form-control rounded-pill" required />
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" id="passwd2" name="repassword" placeholder="Repetir Contraseña" className="form-control rounded-pill" required />
                                                </div>
                                                {/* Select */}
                                                <div className="form-group">
                                                    <select id="country" name="country" className="form-control rounded-pill" onChange={this.onChangeHandler} required><option value="">Seleccione un país</option><option value="United States">United States</option><option value="Canada">Canada</option><option value="Afghanistan">Afghanistan</option><option value="Aland Islands">Aland Islands</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="Andorra">Andorra</option><option value="Angola">Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option><option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Cape Verde">Cape Verde</option><option value="Cayman Islands">Cayman Islands</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option><option value="Cook Islands">Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Cote d'Ivoire">Cote d'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Cura√ßao">Cura√ßao</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guernsey">Guernsey</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option><option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Isle of Man">Isle of Man</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option><option value="Korea, Republic of">Korea, Republic of</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Moldova, Republic of">Moldova, Republic of</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Qatar">Qatar</option><option value="Reunion">Reunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Rwanda">Rwanda</option><option value="Saint Barth&amp;eacute;lemy">Saint Barth&amp;eacute;lemy</option><option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Martin (French part)">Saint Martin (French part)</option><option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia">Serbia</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="South Sudan">South Sudan</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan">Sudan</option><option value="Suriname">Suriname</option><option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="Taiwan">Taiwan</option><option value="Tajikistan">Tajikistan</option><option value="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="Thailand">Thailand</option><option value="Timor-Leste">Timor-Leste</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos Islands">Turks and Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option><option value="Viet Nam">Viet Nam</option><option value="Virgin Islands, British">Virgin Islands, British</option><option value="Wallis and Futuna">Wallis and Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select>
                                                </div>
                                                <div className="form-group">
                                                    <input type="date" id="birth" name="birth" placeholder="Fecha de nacimiento" onChange={this.onChangeHandler} className="form-control rounded-pill" required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" id="phone" name="phone" placeholder="Número de teléfono" onChange={this.onChangeHandler} className="form-control rounded-pill" required/>
                                                </div>
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <button className="btn btn-primary btn-block rounded-pill text-uppercase" onClick={this.onRegisterClick}> 
                                                            Registrarse <i className="fas fa-long-arrow-alt-right"></i>
                                                        </button>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <a href="/login" type="button" className="btn btn-success btn-block rounded-pill text-uppercase">
                                                            Iniciar Sesión<i className="fas fa-long-arrow-alt-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            {/* </form> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Register;