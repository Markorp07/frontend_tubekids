import React, { Component } from 'react';	
import { naxios } from '../Utilities';
import Swal from 'sweetalert2';
import {Modal, Button} from 'react-bootstrap';
class Video extends Component {
    constructor(props) {
        super();
        if(localStorage.getItem("user")){
            let user = JSON.parse(localStorage.getItem("user"));
            var data1 = user.user._id;
        }
        this.state = {
            name:'',
            url:'',
            userID:data1,
            Video: [],
            show : false,
            nameModal: '',
            urlModal:'',
            idModal:'', // This id is from the video that I'm going to update
        };
        // Autobinding
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
        // this.getVideo = this.getVideo.bind(this);
    }
    async componentDidMount(){
        if(!localStorage.getItem("user")){
            window.location.href = "/Login";
        }else{
            this.getVideo().then(res =>{
                const Video = res.data;
                this.setState({Video});
                console.log(Video);
            });
        }
    }
    handleModal(id,name,url){
        this.setState({show:!this.state.show})
        this.setState({idModal:id})
        this.setState({nameModal:name})
        this.setState({urlModal:url})
    }
    onChangeHandler(e){
        const {name,value} = e.target;
        this.setState({...this.state,[name]:value});  
    }
    onCreateClick(){
        if(!this.validateFill()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe rellenar todos los espacios en blanco.'
            });
        }else{
            naxios.post('/video',this.state)
            .then( ({data, status})=>{
                // console.log(data)
            })
            .catch( (err)=>{console.log(err)});
            document.getElementById("name").value = "";
            document.getElementById("url").value = "";
            window.location.href = "/Video";
        }
    }
    editVideo(id){
        // alert(id + " url " + this.state.urlModal);
        var data = {
            name : this.state.nameModal,
            url : this.state.urlModal
        }
        if(!this.validateModalFill()){
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Debe rellenar todos los espacios en blanco.'
            });
        }else{
            return naxios.put('/video'+"/"+id, data)
                .then(function(response){
                    window.location.href = "/Video";
                    return response;
                })
                .catch(function(error){
                    return error;
                });
            }
    }
    deleteVideo(id){
        Swal.fire({
            title: '¿Seguro que desea eliminar el vídeo seleccionado?',
            text: "Esta acción es irreversible.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Borrar'
          }).then((result) => {
            if (result.value) {
              Swal.fire(
                '¡Borrado!',
                'El vídeo fue borrado correctamente.',
                'success'
              )
            //   {console.log(id)}
                return naxios.delete('/video'+"/"+id)
                .then(function(response){
                    window.location.href = "/Video";
                    return response;
                })
                .catch(function(error){
                    return error;
                });
            }
          })
    }
    getVideo(){
        let user = JSON.parse(localStorage.getItem("user"));
        var id = user.user._id;
        return naxios.get('/video'+"/"+id)
            .then(function(response){
                return response;
            })
            .catch(function(error){
                return error;
            });
    }
    
    validateModalFill(){
        let nameModal = document.getElementById("nameModal").value;
        let urlModal = document.getElementById("urlModal").value;
        if(nameModal === "" || urlModal === "" ){
            return false;
        }else{
            return true;
        }
    }
    validateFill(){
        let name = document.getElementById("name").value;
        let url = document.getElementById("url").value;
        if(name === "" || url === "" ){
            return false;
        }else{
            return true;
        }
    }
    render() {
        const round_style = {
            border:0,
            borderRadius: "1rem"
        };
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="text-center">Videos</h2>
                            <hr className="my-4" />
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="card my-2 card shadow-lg mb-5 bg-white" style={round_style}>
                                <div className="card-body">
                                    <h2 className="text-center mb-4">Agregar Vídeos</h2 >
                                    {/* <form > */}
                                        <div className="form-group">
                                            <input type="text" id="name" name="name" placeholder="Nombre del vídeo" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="text" id="url" name="url" placeholder="Url" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                                        </div>
                                        <button className="btn btn-primary btn-block rounded-pill text-uppercase" onClick={this.onCreateClick}> 
                                        Agregar 
                                        </button>
                                    {/* </form> */}
                                </div>
                            </div>
                        </div>
                        {/* Table list */}
                        <div className="col-md-7">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Url</th>                                      
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.Video.map(video=>
                                        <tr>
                                            <td>{video._id}</td>
                                            <td>{video.name}</td>
                                            <td>{video.url}</td>
                                            <td><button id="edit" type="button" className="btn btn-info" onClick={()=>{this.handleModal(video._id,video.name,video.url)}}>Editar</button></td>

                                            <td><button id="delete" type="button" className="btn btn-danger" onClick={this.deleteVideo.bind(this, video._id)}>Eliminar</button></td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                        {/* Modal */}
                        <Modal show={this.state.show} onHide={()=>this.handleModal()}>
                            <Modal.Header closeButton>
                                <h3 className="text-center">Actualizar Datos</h3>
                            </Modal.Header>
                            <Modal.Body>
                            <div className="form-group">
                                <input type="text" id="nameModal" value={this.state.nameModal} name="nameModal" placeholder="Nombre del vídeo" className="form-control rounded-pill" onChange={this.onChangeHandler} />
                            </div>
                            <div className="form-group">
                                <input type="text" id="urlModal" value={this.state.urlModal} name="urlModal" placeholder="Url" className="form-control rounded-pill" onChange={this.onChangeHandler}/>
                            </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button className="btn btn-success btn-block rounded-pill text-uppercase" onClick={this.editVideo.bind(this, this.state.idModal)}>Actualizar</Button>
                                <Button className="btn btn-danger btn-block rounded-pill text-uppercase" onClick={()=>{this.handleModal()}}>Cancelar</Button>
                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Video