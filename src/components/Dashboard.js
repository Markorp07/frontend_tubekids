import React, { Component } from 'react';

class Dashboard extends Component {
    constructor(props) {
        super();
        // Autobinding
    }
    componentDidMount(){
        if(!localStorage.getItem("user")){
            window.location.href = "/Login";
        }else{
            let user = JSON.parse(localStorage.getItem("user"));
            console.log(user.user.name);
        }
    }
 
    render() {
        const round_style = {
            border:0,
            borderRadius: "1rem"
        };
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card my-5 card shadow-lg mb-5 bg-white" style={round_style}>
                        <div className="card-body">
                            <h2 className="card-title text-center">Perfiles</h2>
                            <hr className="my-4" />
                            <a href="/kid" type="button" className="btn btn-success btn-block rounded-pill text-uppercase">Administrar</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card my-5 card shadow-lg  mb-5 bg-white" style={round_style}>
                        <div className="card-body">
                            <h2 className="card-title text-center">Videos</h2>
                            <hr className="my-4" />
                            <a href="/video" type="button" className="btn btn-success btn-block rounded-pill text-uppercase">Administrar</a>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default Dashboard